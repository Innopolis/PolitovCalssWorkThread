package innopolis.politov;

import javax.xml.crypto.Data;
import java.sql.Date;
import java.sql.Time;

public class Main {
    /*
    1. Напишите программу, которая каждую секунду отображает на экране данные о
    времени, прошедшем от начала сессии, а другой ее поток выводит сообщение каждые 5 секунд.
     Предусмотрите возможность ежесекундного оповещения потока, воспроизводящего сообщение, потоком,
     отсчитывающим время.
    2. Не внося изменений в код потока-"хронометра" , добавьте еще один поток,
    который выводит на экран другое сообщение каждые 7 секунд. Предполагается использование методов wait(), notifyAll().
     */



    public static long beginingTime = System.currentTimeMillis();
    public static Object lock = new Object();
    public static boolean redy  = false;

    public static void main(String[] args) {
        Thread threadOneSecond = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    synchronized (lock) {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        lock.notifyAll();
                    }
                    System.out.println(System.currentTimeMillis() - beginingTime);

                }
            }
        });

        Thread threadFiveSecond = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    int counter  = 0;
                    while (counter<5) {
                        synchronized (lock){
                            try {
                                lock.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            counter++;
                        }
                    }
                    System.out.println("After 5 seconds");
                }
            }
        });

        Thread threadSevenSecond = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    int counter  = 0;
                    while (counter<7) {
                        synchronized (lock){
                            try {
                                lock.wait();
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            counter++;
                        }
                    }
                    System.out.println("After 7 seconds");
                }
            }
        });

        threadOneSecond.start();
        threadFiveSecond.start();
        threadSevenSecond.start();


    }
}
